# Infinity Game - Marcelo Penna Test


## Ideia do jogo

Replicar o Jogo recebido no PDF com a maior fidelidade possível, o Jogo foi feito com a biblioteca Phas3r.JS, o jogo foi feito utilizando parcel-bundler e baseado em JAVASCRIPT Moderno para HTML5

## Onde ficam os arquivos Jogáveis

A build jogável fica na pasta DIST, porém é necessário abrir um servidor local por conta de CORS

## Bugs encontrados

Não sei se todos são bugs, Um detalhe que achei estranho e executei diferente foi o Número da Fase, que no projeto da Unity, no seletor de nível começa em 1, e nos níveis começava em 0

Outro Bug q encontrei, foi quando vários níveis são adicionados, as caixas de seleção ultrapassam os limites da tela, esse porém, não resolvei na minha versão

## Explicação básica de scripts

### src/scenes

#### AudioManajer.js

Arquivo de Cena do Phaser 3 que controla a reprodução do áudio

#### EndingScene.js

Cena final do jogo com mensagem de "todas as fases foram completas"

#### LevelManager.js

Cena que gerencia os níveis de jogo, cada fase é essa mesma cena, com os níveis sendo gerados baseados no json **levelData**

#### LevelSelector.js

Cena que gerencia a seleção de níveis

#### MenuScene.js 

Cena inicial do jogo

### src/helpers

#### GeneralConfig.js 

Configurações básicas para criação da classe PHASER

#### LevelControler.js

Scripts auxiliáres, para controle de troca de cena

#### levelData.js

JSON que fica registrado os níveis

## public

Arquivos estáticos
