import Phaser from 'phaser'

import gameConfig from './helpers/generalConfig'
import MenuScene from './scenes/MenuScene'
import LevelSelector from './scenes/LevelSelector'
import AudioManager from './scenes/AudioManager'
import LevelManager from './scenes/LevelManager'
import EndingScene from './scenes/EndingScene'

const config = {
	type: Phaser.AUTO,
	width: gameConfig.width,
	height: gameConfig.height,
	scene: [MenuScene, AudioManager, LevelManager, LevelSelector, EndingScene],
	//scene: [EndingScene],
}

export default new Phaser.Game(config)
