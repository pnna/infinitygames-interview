import Game from '../main'
import gameConfig from "./generalConfig";
import levelData from '../helpers/levelData.json';

export default {

    //funcão para abrir nivel selecionado no seletor
    setLevel : (i) => {

        if(i <= gameConfig.lastLevel + 1){
            gameConfig.actualLevel = i;
            var levelScene = Game.scene.getScene('levelManager');
            var menuScene = Game.scene.getScene('levelSelector');
            menuScene.scene.stop();
            levelScene.scene.stop();
            levelScene.scene.start();
        }
    },
    //função para ir pro próximo nível
    nextLevel : () => {

        gameConfig.lastLevel = levelData[gameConfig.actualLevel].index - 1;
        gameConfig.actualLevel = levelData[gameConfig.actualLevel].index;

        console.log(gameConfig.actualLevel, levelData.length);

        if(gameConfig.actualLevel >= levelData.length){
            console.log(gameConfig.actualLevel, levelData.length);
            return false;
        }

        var levelScene = Game.scene.getScene('levelManager');

        return true;
    },
    //função de jogo completo
    gameComplete : () => {
        console.log( "jogo completo")
        var levelScene = Game.scene.getScene('levelManager');
        levelScene.scene.switch('gameEnding');
    }
}