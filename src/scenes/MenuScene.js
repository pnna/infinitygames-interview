import Phaser from 'phaser'
import gameConfig from '../helpers/generalConfig'
import level from '../helpers/LevelControler'

import Game from '../main'
import AudioManager from './AudioManager'

export default class HelloWorldScene extends Phaser.Scene{

	constructor()
	{
		super('menuScene')
        this.titleText = [];
	}

    create(){
        //instancia a classe de gerenciamento de áudio
        Game.scene.start('audioManager');

        //altera a cor de fundo da página
        this.cameras.main.setBackgroundColor('#23BF8E');

        //textos do menu
        this.titleText[0] = this.add.text(-1, gameConfig.height/ 2  - 70, 'Road', { fontFamily: 'outerFont', fontSize: 40, resolution: 2, shadow: { offsetX: -2, offsetY: 4, color: 'rgb(0 0 0 / 55%)', fill: true, blur: 0 }})
        .setOrigin(0.5)
        .setAlign('center');
        this.titleText[0].setX(-this.titleText[0].width / 2);
        this.titleText[0].setPadding(10,20)

        this.titleText[1] = this.add.text(1, gameConfig.height/ 2 - 20, 'Connect', {fontFamily: 'outerFont', fontSize: 40, resolution: 2, shadow: { offsetX: -2, offsetY: 4, color: 'rgb(0 0 0 / 55%)', fill: true, blur: 0 }})
        .setOrigin(0.5)
        .setAlign('center');
        this.titleText[1].setX(gameConfig.width + this.titleText[1].width);
        this.titleText[1].setPadding(10,20)


        //botão de play
        this.playText = this.add.text(gameConfig.width /2, gameConfig.height - 70, 'Play', {fontFamily: 'filledFont', fontSize: 25, resolution: 2, shadow: { offsetX: -2, offsetY: 4, color: 'rgb(0 0 0 / 55%)', fill: true, blur: 0 }})
        .setOrigin(0.5)
        .setAlign('center');
        this.playText.setPadding(10,30);
        this.playText.setScale(0);
        this.playText.setInteractive();

        //interação de botão play
        this.playText.on('pointerup', () => {
            this.tweens.add(
                {
                    targets: this.playText,
                    scale: 1,
                    duration: 100,
                    onComplete: () => {
                        Game.scene.getScene('audioManager').click();
                        Game.scene.switch('menuScene','levelSelector') 
                    }
                }
            );
        });
        this.playText.on('pointerdown', () => {
            this.tweens.add(
                {
                    targets: this.playText,
                    scale: 1.2,
                    duration: 100
                }
            );
        });

        //Animções 
        this.tweens.add(
            {
                targets: this.titleText[0],
                x: gameConfig.width / 2,
                ease: 'Sine.easeOut',
                duration: 300,
                delay: 200
            }
        );
        this.tweens.add(
            {
                targets: this.titleText[1],
                x: gameConfig.width / 2,
                ease: 'Sine.easeOut',
                duration: 300,
                delay: 200
            },
        );
        this.tweens.add(
            {
                targets: this.playText,
                scale: 1,
                duration: 400,
                delay: 400
            }
        );        
    }
}