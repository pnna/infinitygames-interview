import Phaser from 'phaser'

export default class AudioManager extends Phaser.Scene
{
	constructor()
	{
		super('audioManager')
	}

	preload()
    {

        //Preload de assets de áudio
        this.load.audio("backgroundMusic", ["Music/backgroundMusic.ogg","Music/backgroundMusic.mp3"]);
        this.load.audio("shapeAppear", ["SFX/ShapeAppear.ogg","SFX/ShapeAppear.mp3"]);
        this.load.audio("rotateShape", ["SFX/RotateShape.ogg", "SFX/RotateShape.mp3"]);
        this.load.audio("defaultClick", ["SFX/DefaultClick.ogg", "SFX/DefaultClick.mp3"]);
        this.load.audio("levelComplete", ["SFX/LevelComplete.ogg", "SFX/LevelComplete.mp3"]);
    }

    create()
    {
        //Adiciona na cena os assets
        this.backgroundMusic = this.sound.add("backgroundMusic", {volume: 0, loop: true});
        this.shapeAppear = this.sound.add("shapeAppear");
        this.rotateShape = this.sound.add("rotateShape");
        this.defaultClick = this.sound.add("defaultClick");
        this.levelComplete = this.sound.add("levelComplete");

        //Chama a função que toca a música de fundo
        this.music();
    }

    //funções para tocar os
    music(){
        this.backgroundMusic.play();
        this.tweens.add({
            targets: this.backgroundMusic,
            volume: 0.25,
            duration: 1000
        })
    }
    level(){
        this.levelComplete.play();
    }

    shapeAudio(){
        this.rotateShape.play();
    }
    shape(){
        this.shapeAppear.play();
    }

    click(){
        this.defaultClick.play();
    }


}
