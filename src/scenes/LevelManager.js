import Phaser from 'phaser'
import levelData from '../helpers/levelData.json'
import gameConfig from '../helpers/generalConfig'
import Game from '../main'
import AudioManager from './AudioManager'
import LevelControler from '../helpers/LevelControler'

export default class LevelManager extends Phaser.Scene
{
	constructor()
	{
		super('levelManager')
	}

	preload()
    {
        //preload de assets
        this.load.image('hamburguerMenu', './hamburguer.png');

        for(var i = 1; i <= 7; i++){
            this.load.image(`road_${i}`, `./roads/${i}.png`);
        }
    }

    create()
    {

        //cor de fundo
        this.cameras.main.setBackgroundColor('#23BF8E');

        //carrega o nível atual
        let actualLevel = levelData[gameConfig.actualLevel].data;
        this.level = [];

        var container = this.add.container(0,0);

        //looping pelas peças do nível
        for(let i = 0; i < actualLevel.length; i++){

            for(let a = 0; a < actualLevel[i].length; a++){

                console.log(actualLevel[i][a].tile);
                if(actualLevel[i][a].tile != null){
                    
                    let actualSprite = this.add.image(-(gameConfig.roadSize + gameConfig.roadSize / 2) + gameConfig.roadSize * a, -(gameConfig.roadSize + gameConfig.roadSize / 2) + gameConfig.roadSize * i, actualLevel[i][a].tile)
                    .setOrigin(0.5)
                    .setRotation(Phaser.Math.DegToRad(actualLevel[i][a].start))
                    .setDisplaySize(0,0)
                    .setInteractive();

                    this.tweens.add({
                        targets: actualSprite,
                        duration: 200,
                        delay : 200 + (100 * Phaser.Math.Between(3,7)),
                        displayWidth: gameConfig.roadSize,
                        displayHeight: gameConfig.roadSize,
                        sine: 'Sine.easeOut',
                        onStart: () => {Game.scene.getScene('audioManager').shape()}

                    })

                    //aplica a função de rotação do sprite
                    actualSprite.on('pointerdown', (e)=> {
                        this.rotateSprite(actualSprite);
                    });

                    //adiciona na var pra poder verificar o sucesso do nível
                    this.level.push({sprite: actualSprite, solution: actualLevel[i][a].solution});
                    container.add(actualSprite);
                }
            }
        }

        this.spriteMoving = false;

        container.setPosition(gameConfig.width/2, gameConfig.height/2);
        
        //Título da fase
        this.leveltitle = this.add.text(gameConfig.width/2, 30, `Level ${levelData[gameConfig.actualLevel].index}`,{fontFamily: 'filledFont', fontSize: 25, resolution: 2, shadow: { offsetX: -2, offsetY: 4, color: 'rgb(0 0 0 / 55%)', fill: true, blur: 0 }})
        .setOrigin(0.5)
        .setPadding(10,30);

        this.leveltitle.setX(gameConfig.width + this.leveltitle.width / 2);

        //slide do título
        this.tweens.add({
            targets: this.leveltitle,
            duration: 200,
            x: gameConfig.width / 2,
            ease: 'Sine.easeOut'
        });


        //botão para acessar o menu
        let hamburguer = this.add.image( gameConfig.width / 2, gameConfig.height - 40, 'hamburguerMenu')
        .setInteractive()
        .setScale(.4,.4);

        hamburguer.on('pointerdown', ()=> {
            Game.scene.getScene('audioManager').click();
            hamburguer.setTint(0xdddddd)
        });
        hamburguer.on('pointerup', ()=>{
            hamburguer.setTint(0xffffff);
            this.scene.switch('levelSelector');
        });
    }

    //rotação do sprite
    rotateSprite(sprite){

        if(!this.spriteMoving){
            let degree = sprite.angle;
            degree += 90;

            this.tweens.add({
                targets: sprite, 
                duration: 150, 
                angle: degree,
                onStart: ()=> {
                    this.spriteMoving = true;
                    Game.scene.getScene('audioManager').shapeAudio();
                },
                onComplete: ()=> this.verifyGame(),
            })
        }
                    
    }

    //valida se a fase está concluida
    verifyGame(){
        this.spriteMoving = false;
        let success = true;

        console.log(this.level);
        this.level.forEach((sprite) =>{


            if(sprite.solution.length > 1){
                    if(!sprite.solution.includes(sprite.sprite.angle)){
                        success = false;
                    }
            }
            else if(sprite.sprite.angle != sprite.solution){
                success = false;
            }

        });

        if(success)
            this.levelComplete();
    }

    //Valida e passa pra próxima fase
    levelComplete(){

        this.spriteMoving = true;

        this.level.forEach((sprite) => {
            this.tweens.add({
                targets: sprite.sprite,
                scale: 0,
                duration: 200,
                delay: 400,
                ease: 'Sine.easeOut',
                onStart: ()=> {
                    Game.scene.getScene('audioManager').level();
                }
            });
        });
        
        this.tweens.add({
            targets: this.leveltitle,
            duration: 200,
            delay: 1200,
            ease: 'Sine.easeOut',
            x: -this.leveltitle.width/2,
            onComplete: () => {
                if(LevelControler.nextLevel() == false){
                    LevelControler.gameComplete();
                }else{
                    this.scene.restart();
                }
            }
        });
    }
}
