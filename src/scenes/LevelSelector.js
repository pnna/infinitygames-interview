import Phaser from 'phaser';
import gameConfig from '../helpers/generalConfig';
import Game from '../main';
import levelData from '../helpers/levelData.json';
import LevelControler from '../helpers/LevelControler';

export default class LevelSelector extends Phaser.Scene
{
	constructor()
	{
		super('levelSelector')
	}

    //preload de assets
	preload()
    {
        this.load.image("orangeButton", "./action_button.png")
        this.load.image("orangeButton_", "./action_button_.png")
        this.load.image("grayButton", "./lock_button.png")
        
    }

    create()
    {
        //cor de fundo da tela
        this.cameras.main.setBackgroundColor('#23BF8E');

        let column = 0;
        let line = 0;

        //configuração de fundo de tela da seleção de nível
        let container = this.add.container(gameConfig.width/2,gameConfig.height/2);
        
        this.leveltitle = this.add.text(gameConfig.width/2, 30, `Level Select`,{fontFamily: 'filledFont', fontSize: 25, resolution: 2, shadow: { offsetX: -2, offsetY: 4, color: 'rgb(0 0 0 / 55%)', fill: true, blur: 0 }})
        .setOrigin(0.5)
        .setPadding(10,30);

        let backgroundRect = this.add.rectangle(0, 0, gameConfig.width - 80, gameConfig.height * .75, 0xffffff)
        .setAlpha(0.4)
        .setOrigin(.5);
        
        container.add(backgroundRect);

        //loop pra exibir os níveis no seletor
        levelData.forEach((e,index) => {
            column = Math.floor(index / 4)
            line = Math.floor(index%4)

            let x = -gameConfig.roadSize /2 + -gameConfig.roadSize + (gameConfig.roadSize * line);
            let y = -gameConfig.height * .75 / 2 + 60 + gameConfig.roadSize * column;

            let actualButton = this.add.image( x, y, "orangeButton" )
            .setOrigin(.5)
            .setDisplaySize(gameConfig.roadSize * .75, gameConfig.roadSize * .75);
            container.add(actualButton);

            let buttonText = this.add.text(x + 2,y - 2,`${index}`,{fontFamily: 'filledFont', fontSize: 25, resolution: 2, shadow: { offsetX: -2, offsetY: 4, color: 'rgb(0 0 0 / 55%)', fill: true, blur: 0 }})
            .setOrigin(0.5)
            .setPadding(10,30);
            container.add(buttonText);

            actualButton.setInteractive();

            if(index > gameConfig.lastLevel){
                actualButton.setTint(0xaaaaaa);
                actualButton.setTexture('grayButton')

                return false;
            }
            actualButton.on('pointerdown', () => {
                actualButton.setTexture('orangeButton_')
                actualButton.y += 4;
                buttonText.y += 4;
                Game.scene.getScene('audioManager').click()
            });
            actualButton.on('pointerup', ()=> {
                actualButton.setTexture('orangeButton')
                actualButton.y -= 4;
                buttonText.y -= 4;
                LevelControler.setLevel(index);
            });
            
        })

    }
}
