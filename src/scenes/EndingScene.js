import Phaser from 'phaser'
import levelData from '../helpers/levelData.json'
import gameConfig from '../helpers/generalConfig'
import Game from '../main'
import AudioManager from './AudioManager'
import LevelControler from '../helpers/LevelControler'

export default class EndingScene extends Phaser.Scene
{
	constructor()
	{
		super('gameEnding')
	}

    //preload de assets
	preload()
    {
        this.load.image('hamburguerMenu', './hamburguer.png');

        for(var i = 1; i <= 7; i++){
            this.load.image(`road_${i}`, `./roads/${i}.png`);
        }
    }

    create()
    {
        //cor de fundo
        this.cameras.main.setBackgroundColor('#23BF8E');

        //texto de final de fase
        this.endText = this.add.text(gameConfig.width/2, gameConfig.height /2, "All\nlevels\ncleared\n= )",{fontFamily: 'filledFont', fontSize: 25, resolution: 2, lineSpacing: 35,align: "center", shadow: { offsetX: -2, offsetY: 4, color: 'rgb(0 0 0 / 55%)', fill: true, blur: 0 }})
        .setOrigin(0.5)
        .setPadding(10,30)
        .setAlpha(0);

        //Animação
        this.tweens.add({
            targets: this.endText,
            duration: 400,
            delay:200,
            alpha: 1,
            onstart: ()=> {
                Game.scene.getScene('audioManager').level();
            },
            ease: 'Sine.easeOut'
        });

        //botão para acessar o menu
        let hamburguer = this.add.image( gameConfig.width / 2, gameConfig.height - 40, 'hamburguerMenu')
        .setInteractive()
        .setScale(.4,.4);

        hamburguer.on('pointerdown', ()=> {
            hamburguer.setTint(0xdddddd)
        });
        hamburguer.on('pointerup', ()=>{
            hamburguer.setTint(0xffffff);
            this.scene.switch('levelSelector');
        });
    }
}
